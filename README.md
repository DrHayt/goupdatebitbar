![Logo](docs/header.png)

# GoUpdate BitBar

* [Overview](#markdown-header-overview)
* [Install](#markdown-header-install)
* [Contributors](#markdown-header-contributors)


## Overview

GoUpdateBitBar is plugin for [BitBar](https://getbitbar.com/) that will check the website [https://golang.org/dl/](https://golang.org/dl/) and your current version to see if you need to update.



## Install

### Requirements

* BitBar needs to be installed ... kinda a given but now you know.
* Go needs to be intalled in the path specified by the install instructions at golang.org (/usr/local/go/bin).

### Download

visit the [downloads](https://bitbucket.org/krakencode/goupdatebitbar/downloads/) section and get the lattest version. Save the file to your plugins directory.

## Contributors
